package com.hendisantika

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootKeycloak4Application

fun main(args: Array<String>) {
    runApplication<SpringBootKeycloak4Application>(*args)
}
