package com.hendisantika.service

import org.keycloak.admin.client.Keycloak
import org.keycloak.representations.idm.GroupRepresentation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-keycloak4
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/05/22
 * Time: 21.36
 */
@Service
class GroupService(
    private val keycloak: Keycloak,
    @Value("\${keycloak.realm}")
    private val realm: String
) {
    fun create(name: String) {
        val group = GroupRepresentation()
        group.name = name

        keycloak
            .realm(realm)
            .groups()
            .add(group)
    }

    fun findAll(): List<GroupRepresentation> =
        keycloak
            .realm(realm)
            .groups()
            .groups()
}
