package com.hendisantika.controller

import com.hendisantika.service.RoleService
import org.springframework.web.bind.annotation.*

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-keycloak4
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/05/22
 * Time: 21.44
 */
@RestController
@RequestMapping("/api/role")
class RoleController(
    private val roleService: RoleService
) {
    @GetMapping
    fun findAll() =
        roleService.findAll()

    @PostMapping
    fun createRole(@RequestParam name: String) =
        roleService.create(name)
}
