package com.hendisantika.controller

import com.hendisantika.service.GroupService
import org.springframework.web.bind.annotation.*

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-keycloak4
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/05/22
 * Time: 21.43
 */
@RestController
@RequestMapping("/api/group")
class GroupController(
    private val groupService: GroupService
) {
    @GetMapping
    fun findAll() =
        groupService.findAll()

    @PostMapping
    fun createGroup(@RequestParam name: String) =
        groupService.create(name)

}
